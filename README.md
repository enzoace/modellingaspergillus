# ModellingAspergillus

Supporting material to: 'Modelling approaches reveal new regulatory networks in Aspergillus fumigatus metabolism', The Journal of Fungi, 2020.